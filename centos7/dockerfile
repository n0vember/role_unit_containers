FROM centos:7

MAINTAINER n0vember <n0vember@half-9.net>

### A bit of environment

ENV container docker
ENV TERM xterm

### Copy files in place

COPY is_ready /usr/local/bin/is_ready

### All RUN in one layer to gain space

RUN echo "===== YUM INSTALLATION AND CLEANUP =====" && \
    yum -y update && \
    yum install -y sudo vim git epel-release uuid && \
    echo "===== SYSTEMD PRIVILEGED PROTECTION =====" && \
    ( \
      cd /lib/systemd/system/sysinit.target.wants/ && \
      rm -f $(ls | grep -v systemd-tmpfiles-setup.service) \
    ) && \
    rm -f /lib/systemd/system/multi-user.target.wants/* && \
    rm -f /etc/systemd/system/*.wants/* && \
    rm -f /lib/systemd/system/local-fs.target.wants/* && \
    rm -f /lib/systemd/system/sockets.target.wants/*udev* && \
    rm -f /lib/systemd/system/sockets.target.wants/*initctl* && \
    rm -f /lib/systemd/system/basic.target.wants/* &&\
    rm -f /lib/systemd/system/anaconda.target.wants/* &&\
    rm -f /usr/lib/tmpfiles.d/systemd-nologin.conf && \
    rm -f /lib/systemd/system/systemd*udev* && \
    rm -f /lib/systemd/system/getty.target && \
    echo "===== ENABLE IS_READY SCRIPT =====" && \
    chmod +x /usr/local/bin/is_ready && \
    echo "===== CLEANUP =====" && \
    yum clean all

### define working directory
WORKDIR /workdir

### define systemd compatible stop signal
STOPSIGNAL SIGRTMIN+3

### Start systemd, be a system
CMD ["/usr/lib/systemd/systemd"]

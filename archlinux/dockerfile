FROM archlinux:latest

MAINTAINER n0vember <n0vember@half-9.net>

### A bit of environment

ENV container docker
ENV TERM xterm
ENV DIND_COMMIT 3b5fac462d21ca164b3778647420016315289034

### Copy files in place

COPY is_ready /usr/local/bin/is_ready
COPY mirrorlist /etc/pacman.d/mirrorlist

### All RUN in one layer to gain space

RUN echo "===== APT INSTALLATION AND CLEANUP =====" && \
    pacman --sync --refresh --noconfirm --noprogressbar archlinux-keyring && \
    pacman --sync --refresh --sysupgrade --noconfirm --noprogressbar && \
    pacman --sync --noconfirm --noprogressbar \
        systemd \
        git\
        sudo \
        iproute2 \
        net-tools \
        curl \
        python \
        util-linux \
    && \
    echo "===== SYSTEMD PRIVILEGED PROTECTION =====" && \
    ( \
      cd /lib/systemd/system/sysinit.target.wants/ && \
      rm -f $(ls | grep -v systemd-tmpfiles-setup.service) \
    ) && \
    rm -f /lib/systemd/system/multi-user.target.wants/* && \
    rm -f /etc/systemd/system/*.wants/* && \
    rm -f /lib/systemd/system/local-fs.target.wants/* && \
    rm -f /lib/systemd/system/sockets.target.wants/*udev* && \
    rm -f /lib/systemd/system/sockets.target.wants/*initctl* && \
    rm -f /lib/systemd/system/basic.target.wants/* &&\
    rm -f /lib/systemd/system/anaconda.target.wants/* &&\
    rm -f /usr/lib/tmpfiles.d/systemd-nologin.conf && \
    rm -f /lib/systemd/system/systemd*udev* && \
    rm -f /lib/systemd/system/getty.target && \
    echo "===== ENABLE DOCKER IN DOCKER =====" && \
    set -x && \
    groupadd --system dockremap && \
    useradd --system -g dockremap dockremap && \
    echo 'dockremap:165536:65536' >> /etc/subuid && \
    echo 'dockremap:165536:65536' >> /etc/subgid && \
    curl -k -o /usr/local/bin/dind "https://raw.githubusercontent.com/docker/docker/${DIND_COMMIT}/hack/dind" && \
    chmod +x /usr/local/bin/dind && \
    echo "===== ENABLE IS_READY SCRIPT =====" && \
    chmod +x /usr/local/bin/is_ready && \
    echo "===== SOME GENERAL SYSTEM CLEANUP =====" && \
    echo "pacman --remove --noconfirm man-pages" && \
    pacman --sync --clean --clean && \
    rm -rf /var/cache/pacman/pkg && \
    rm -rf /usr/share/locale/* && \
    rm -rf /usr/share/doc/* && \
    find / -name "*.pyc" -delete && \
    find / -name "__pycache__" -delete && \
    echo "===== DONE ====="

### expose Docker in docker port
EXPOSE 2375

### define working directory
WORKDIR /workdir

### define systemd compatible stop signal
STOPSIGNAL SIGRTMIN+3

### Start systemd, be a system
CMD ["/lib/systemd/systemd"]
